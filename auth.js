const jwt = require ('jsonwebtoken');
//User defined string data that will be used to create our JSON webtokens
//Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = 'CourseBookingAPI';

//JSON web token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
//Information is kept secure through the use of the secret code
//only the system knows the secret code that can decode the encrypted information
//secret is equal to a lock code.

//Token Creation
//Analogy = Pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) => {
	//The data will be received from the registration form
	//When the user logs in, a token will be created with user's information

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	//Generate a JSOn webtoken using the jwt's sign method(signature)
	//Generates the token using form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})
}

//Token verification
//Analogy- Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with.

module.exports.verify = (req,res,next) => {
	//The token is retrieved from the request header
	//This can be provided in postman under
		//authorization > Bearer Token
	let token = req.headers.authorization;

	//Token received and is not undefined
	if(typeof token !== "undefined"){
		// console.log(token);
		//The token that we receive is just like this:
			//"Bearer kasdasd312312dfaseadwdasddfgdasfgsxtyojdyasdasdasd"
			//7 characters + space remove using "slice"
		//The "Slice" method only takes the token from the information sent via the request header
		//This removes the "Bearer" prefix and obtains only the token for verification
		token = token.slice(7, token.length)

		//Validate the token using "verify" method decrypting the token using secret code

		return jwt.verify(token, secret,(err, data) =>{
			//if JWT is not valid
			if(err){
				return res.send({auth: "Failed"})
			}else{
			//If JWT is valid
			//Allows the application to proceed with the next middleware function/callback function in the route
			//the next() middleware will be used to proceed to another function that invokes the controller function(business logic)
				next()

			}
		})
	}else{
		//Token does not exist
		return res.send({auth: "Failed"});
	}
}

//Token decryption
//Analogy- Open the gift and get the content
module.exports.decode = (token) => {

	//Token received and is not undefined
	if(typeof token !== "undefined"){

		//Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7,token.length);

		return jwt.verify(token,secret, (err,data) =>{
			if(err){
				return null;
			}else{
				//The "decode" method is used to obtain the info from the JWT
				//{complete:true} option that allows us to return additional info from the JWT token
				//Returns an object with access to the "payload" property user information stored when the token was generated
				//The payload contains the info provided in the "createAccessToekn" method defined above (id,email, and isAdmin)
				return jwt.decode(token, {complete:true}).payload
				}

			})
		//Token does not exist
	}else{
		return null;
	}
}

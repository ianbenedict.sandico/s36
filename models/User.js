const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'first name is required']
	},
	lastName: {
		type: String,
		required: [true, 'last name is required']
	},
	email: {
		type: String,
		required: [true, 'email is required']
	},
	password: {
		type: String,
		required: [true, 'password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile No is required']
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required']
			},
			enrolledOn:{
				type: Date,
				default: new Date(),
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
})

module.exports = mongoose.model('User', userSchema);

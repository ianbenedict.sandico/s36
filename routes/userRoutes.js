const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');	
const auth = require("../auth");



//Route for checking if the user's email aready exists in the database
router.post('/checkEmail', (req,res) =>{
	userController.checkEmailExists(req.body).then(result => res.send(result));

})

//Route for user registration
router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result));

})

//Route for user authentication	
router.post('/login', (req,res) =>{
	userController.loginUser(req.body).then(result => res.send(result));
})

//The auth.verify acts as a middleware to ensure that the user is logged in before they can access their profile
router.get('/details', auth.verify, (req,res) => {
	//Uses the "decode" method defined in the 'auth.js' fil to retrieve the user information from the token passing the "token" from the request header as an argument

	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(result => res.send(result));
})


//Route to enroll a user to a course
router.post('/enroll', auth.verify, (req,res) =>{
	let data = {
		//user ID will retrieve from the request header
		userId: auth.decode(req.headers.authorization).id,
		//Course Id will be retrieved from the request body
		courseId: req.body.courseId
	}
	userController.enroll(data).then(result =>res.send(result))
})



module.exports = router;
const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth');

//Route for creating a course
//localhost:4000/courses/
router.post('/', auth.verify, (req,res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(result => res.send(result));
})

//Retrieve all courses
router.get('/all', (req,res) =>{
	courseController.getAllcourses().then(result => res.send(result));
})

//Retrieve all TRUE courses
router.get('/',(req,res) =>{
	courseController.getAllActive().then(result =>res.send(result));
})

//Retrieve SPECIFIC course

router.get("/:Id", (req,res) => {
	courseController.specificCourse(req.params).then(result =>res.send(result));
})

//Update a course
router.put("/:courseId", auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})

router.put('/:courseId/archive', auth.verify , (req,res) =>{

	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){	
		courseController.archiveCourse(req.params).then(result =>res.send(result))
	}else{
		res.send(false)
	}
	
}) 

module.exports = router;
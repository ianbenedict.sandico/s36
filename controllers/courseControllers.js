const Course = require('../models/Course');


//Creation of course
/*
1.Create a conditional statement if the use is an admin.
2.Create a new Course object using the mongoose model and the information from the request body and the id from the header
3.Save the new User to the database
*/

module.exports.addCourse =  (data) => {
	//User is an admin
	if(data.isAdmin){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})
		return newCourse.save().then((course, error) =>{
			//Course cration failed
			if(error) {
				return false;

			}else{
				return true
			}
		})
	}else{
		//User is not an admin
		return false;
	}
}

// Retrieve all courses
module.exports.getAllcourses =() => {
	return Course.find({}).then( result =>{
		return result;
	})
}

//Retrieve all ACTIVE courses
module.exports.getAllActive = () =>{
	return Course.find({isActive: true}).then(result =>{
		return result;
	})
}

//Retrieve SPECIFIC course
module.exports.specificCourse = (reqParams) => {
	console.log(reqParams)
	return Course.findById(reqParams.Id).
	then(result =>{
		return result;
	})
}

//Update a course
/*
1.Steps:
1.Check if the user is an admin
2. Create a variable which will contain information retreived from the req.body
3.Find and update the course using the courseId retrieved from the req.params property and the variable containing the information from the quest body
*/
//Update a course

module.exports.updateCourse = (reqParams,reqBody) =>{
	// console.log(reqParams,reqBody)
	let updatedCourse = {		
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//findByIdandUpdate
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) =>{
		// console.log(course)
		if(error){
			return false;
		}else{
			return true;
		}
			// (error)? false : true
	})
}	


//ternary operator(es6 updates)
// if(error){
// 	return false
// }else{
// 	return true
// }

// (error)? false: true


/*
Archive
Steps:
1.Check if admin(routes)
2.Create a variable where the isActive will change into false
3. we can use findByIdandUpdate(id,updatedVariable). Then error handling, if course is not archived, return false, if the cour is archived successfully, return true
*/

//Archive a Course
module.exports.archiveCourse = (reqParams) =>{
	let updatedCourse = {
		isActive: false
	}
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error) =>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}
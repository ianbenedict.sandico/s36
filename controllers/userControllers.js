const User = require('../models/User');
const Course =require('../models/Course')
const bcrypt = require('bcrypt');
const auth = require('../auth');



//Check if the email already exists

/*
Steps:
1.Use mongoose "find" method to find duplicate checkEmailExists	
2.Use the "then" method to use a response back to the client based on the result of the find method

*/

module.exports.checkEmailExists = (reqBody) => {
	//when looking for property, object use curly braces
	return User.find({email: reqBody.email}).then(result =>{
		//The "Find" method returns a recor if a match is found
		if(result.length > 0){
			return true;
		}else{
			//No duplicate email found
			//The user is not yet registered in the database
			return false;
		}

	})
}


//User registration
/*
Steps:
1.Create a new User object using the mongoose model and the information from the request body.
2.Error handling, if error, return error. else, save the new User to the database
*/
module.exports.registerUser = (reqBody) => {
//Uses the information from the request body to proide all the necessary information

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//10 is the value provided  as the number of "salt" rounds that the brupy algorithm will run in order to encrypt the password.
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	//Saves the created object to our database
	return newUser.save().then((user, error) =>{
		//User registration failed
		if(error){
			return false;
		}else{
			//if user registration is successful
			return true;
		}
	})
}

// User authentication
/*
Steps:
1.Check the database if the user email exists
2.Compare the password provided in the login form with the password stored in the database
3.Generate/return a JSOn web token if the user is succesfully logged in and return false if not
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		//If user does not exist
		if(result == null){
			return false;
		}else{
			//user exists
			//Create a variable "isPasswordCorrect" to return the result of compairing the login form password and the database password
			//"compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or false value depending on the result
			//A good practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+ "Noun"
				//ex. isSingle, isDone, isAdmin, areDone, etc.
			const isPasswordCorrect= bcrypt.compareSync(reqBody.password, result.password)
			//If the password match/result of the above code is true
				if(isPasswordCorrect){
					//Generate an access token
					//Use the "createAccessToken" method defined in the "auth.js" file
					//returning an object back to the frontend
					//We will use the mongoose method "toObject" - it converts the mongoose object into a plain javascript object.
					return { accessToken: auth.createAccessToken(result.toObject()) }
				}else{
					//Passwords do not match
				}	return false;
		}
	})
}

module.exports.getProfile = (data) => {

	return User.findById( data.userId).then(result =>{
		result.password = "";
		return result;
	})
}

//Enroll a user to a class/course

/*
Steps:
1.Find the document in the database using the user's ID
2. Add the course ID to the user's enrollment array
3. Find the document in the database using the course's ID
4.Save the data in the database
5. add the users ID to the course's enrolees array
7. save the data in the database
8. Error handling: if successful, return true, else return false

*/
//Assync await will be used in enrolling the user because we will need to update 2 seperate documents when enrolling a user.

module.exports.enroll = async(data) => {
	//Creates an "isUserUpdated" variable and returns true upon successfull update otherwise false
	//Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the cliet
	let isUserUpdated = await User.findById(data.userId).then(user => {
		//Add the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId })
	
		//Saves the updated user information in the database
		return user.save().then((user,error) =>{
			if(error){
				return false;
			}else{
				return true;
			}
		})//the isUserUpdated tells us if the course was successfully added to the user
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{
		//Add the userId in the course's enrolles array
		course.enrollees.push({ userId: data.userId});

		return course.save().then((course,error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})//the is courseUpdated tells us if the user was successfully saved to the course
	})

	//Condition that will check if the user and course documents have been successfully updated
	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{
		//if the user enrollment failed
		return false;
	}
}